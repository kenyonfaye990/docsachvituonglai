# Đọc sách vì tương lai

Chương trình Đọc sách vì tương lai được phát động bởi Hội đồng Đội TW, NXB Giáo dục Việt Nam với mục đích giúp học sinh hình thành thói quen, hiểu được ý nghĩa của việc đọc sách và góp phần phát triển văn hóa đọc trong nhà trường và cộng đồng

- SDT: 0896565123

Câu nhắc hay về cuộc sống đầy nhiệt huyết
một. Cuộc sống khiến bạn buồn chán ư? Hãy lao vào công việc bạn tin tưởng bằng số đông trái tim, sống vì nó, chết vì nó, và bạn sẽ mua thấy thứ hạnh phúc tưởng nghe đâu không bao giờ đạt được.

2. Lòng kiên nhẫn và thời kì khiến cho được rộng rãi hơn là sức mạnh hay nhiệt huyết.

3. Máu nóng thiếu đi kiến thức chỉ là lửa thiếu đi ánh sáng.

Đây là một trong các câu đề cập hay về cuộc sống mà bạn chẳng thể bỏ qua, hãy học hỏi câu nhắc này về sự tâm huyết của mình nhé!

4. Nhiệt huyết là mẹ của quyết tâm, và không có nó, ta chẳng thể đạt được điều gì to lớn.

5. Hãy sống khao khát, hãy sống khờ dại.

6. Thật sai trái lúc cho rằng máu nóng sục sôi nhất là trong tuổi trẻ! Lửa hết dạ ko mạnh hơn, mà là khả năng kiểm soát chúng yếu hơn! Tuổi xanh dễ kích động, mãnh liệt và rõ ràng hơn, nhưng sinh lực, sự dẻo dai, chiều sâu và sức tập trung đều ko bằng được người từng trải.

7. Thà nhiệt huyết và phạm phổ biến sai lầm còn hơn là thận trọng và thiển cận.

8. Sự hy sinh, nhiệt huyết của những linh hồn vĩ đại, chưa bao giờ là lề luật của xã hội loài người.

có câu đề cập hay trong cuộc sống này giúp bạn hiểu thế nào là sự hi sinh, máu nóng và cả các vong linh vĩ đại.

9. Đôi lúc ánh sáng trong cuộc thế ta lịm tắt, và được nhen đội ngũ lại bởi tia lửa của một người nào đấy. Mỗi chúng ta phải nghĩ tới các người đã đốt lên ngọn lửa trong ta có lòng hàm ơn sâu sắc.

10. Không ai già hơn các người đã sống cạn bầu máu nóng.

https://docsachvituonglai.vn/

https://lotus.vn/w/blog/doc-sach-vi-tuong-lai-509636950622208000.htm

https://www.behance.net/docsachvituonglai/info

https://www.flickr.com/people/196871753@N02/
